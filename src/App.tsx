import React from 'react';
import './App.css';

function App() {
  return (
    <div className="Container">
      <h3><span className="Accent">@</span>leandrofabianjr</h3>
      <h1>Leandro Fabian Jr.</h1>
      <h2>web developer</h2>
    </div>
  );
}

export default App;
